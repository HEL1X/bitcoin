-- CREDS ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--[[               ____  ______| dP     dP   88888888b dP        dP dP    dP 
 _      __(_)___  / __ \/ ____/| 88     88   88        88        88 Y8.  .8P 
| | /| / / / __ \/ /_/ /___ \  | 88aaaaa88a a88aaaa    88        88  Y8aa8P  
| |/ |/ / / / / /\__, /___/ /  | 88     88   88        88        88 d8'  `8b
|__/|__/_/_/ /_//____/_____/   | 88     88   88        88        88 88    88 
                               | dP     dP   88888888P 88888888P dP dP    dP 
Script made by win95|HELIX
--]]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- LOAD STATEMENT -------------------

print("HELIX Bitcoin Script loaded!")

-------------------------------------

-- CONFIG OPTIONS -------------------------------------------
local decal = "0" -- The decal that will show up on your sign

-------------------------------------------------------------

-- SERVICES -------------------------------------------------

local HttpService = game:GetService("HttpService")

local ReplicatedStorage = game:GetService("ReplicatedStorage")

--------------------------------------------------------------

-- CHANGENAME FUNCTION ------------------------

function changeName(n)

  local A_1 = "Update"
  
  local A_2 = {
    ["DescriptionText"] = n,
    ["ImageId"] = decal
  }
  local event = ReplicatedStorage.CustomiseBooth

  event:FireServer(A_1, A_2)

end

-----------------------------------------------


-- This block is here twice so that the initial change is fast and then it wait()s. It's a bit iffy but it works.

local response = game:HttpGet("https://api.cryptowat.ch/markets/coinbase-pro/btcusd/price")

local jsonDecoded = HttpService:JSONDecode(response)

local result = jsonDecoded.result

local price = result.price

changeName(string.format("The current price of Bitcoin (BTC) in USD is $%d", price))

---------------------------------------------------------------------------------------------------------------

-- MAIN WHILE LOOP --------------------------------------------------------------------

while wait() do

  wait(3)

  local response = game:HttpGet("https://api.cryptowat.ch/markets/coinbase-pro/btcusd/price")

  local jsonDecoded = HttpService:JSONDecode(response)

  local result = jsonDecoded.result

  local price = result.price

  changeName(string.format("The current price of Bitcoin (BTC) in USD is $%d", price))

  wait(2)

  changeName("This sign automatically updates")

end

----------------------------------------------------------------------------------------

